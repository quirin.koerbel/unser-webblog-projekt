package de.awacademy.test.webblog.comment;

import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CommentController {

    @Autowired
    CommentService commentService;




    @PostMapping("/addComment")
    public String create(Model model, @RequestParam("entryId") long entryId, @ModelAttribute("comment") CommentDTO comment,
                         BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser,
                         RedirectAttributes redirectAttributes) {

        commentService.addNewComment(comment, currentUser, entryId);
        commentService.refreshModelAttributesWithEntryAndComments(model, entryId);

        redirectAttributes.addAttribute("entryId", entryId);
        return "redirect:/entryView";
    }


    @PostMapping("/deleteComment")
    public String deleteComment(Model model, @RequestParam("commentId") long commentId, @RequestParam("entryId") long entryId,
                                @ModelAttribute("currentUser") User currentUser, RedirectAttributes redirectAttributes) {

        commentService.deleteComment(commentId, currentUser);
        commentService.refreshModelAttributesWithEntryAndComments(model, entryId);

        redirectAttributes.addAttribute("entryId", entryId);
        return "redirect:/entryView";
    }




}
