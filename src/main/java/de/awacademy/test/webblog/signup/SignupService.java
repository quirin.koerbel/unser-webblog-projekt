package de.awacademy.test.webblog.signup;

import de.awacademy.test.webblog.user.User;
import de.awacademy.test.webblog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Service
public class SignupService {

    @Autowired
    UserRepository userRepository;

    public void checkInputForErrors(Model model, BindingResult bindingResult, SignupDTO signup){

        if (!signup.getPassword1().equals(signup.getPassword2())) {
            bindingResult.addError(new FieldError("signup", "username", "Passwörter nicht identisch"));
            model.addAttribute("Error", "Die Passwörter stimmen nicht überein");

        }else if (userRepository.existsByName(signup.getUsername())) {
            bindingResult.addError(new FieldError("signup", "username", "Username bereits vergeben"));
            model.addAttribute("Error", "Der Username ist schon vergeben");

        }else if (signup.getPassword1().length() < 5){
            bindingResult.addError(new FieldError("signup", "username", "Das Passwort muss mindestens 5 Zeichen lang sein"));
            model.addAttribute("Error", "Dein Passwort muss mindestens 5 Zeichen lang sein");
        }

    }

    public User saveNewUser(SignupDTO signup){
        User user = new User(signup.getUsername(), signup.getPassword1());

        //make first User in Database to Admin
        if (userRepository.findAllByAdministratorEqualsOrderByName(true).size() == 0) {
            user.setAdministrator(true);
        }

        userRepository.save(user);
        return user;
    }


}
