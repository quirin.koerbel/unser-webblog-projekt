package de.awacademy.test.webblog.entry;

import de.awacademy.test.webblog.user.User;
import de.awacademy.test.webblog.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class EntryController {

    @Autowired
    EntryService entryService;
    @Autowired
    EntryRepository entryRepository;


    @GetMapping("/newEntry")
    public String showEntryEditPage(Model model) {

        model.addAttribute("entry", new EntryDTO());
        model.addAttribute("entryToEdit", new Entry());
        model.addAttribute("categorys", entryService.categoryRepository.getAllByOrderByName());

        return "entry_edit";
    }


    @GetMapping("/editEntry")
    public String editEntry(Model model, @RequestParam("entryToEditId") long entryToEditId) {

        model.addAttribute("categorys", entryService.categoryRepository.getAllByOrderByName());
        entryService.addAttributesToModelForEditingEntry(model, entryToEditId);
        return "entry_edit";
    }


    @Transactional
    @PostMapping("/deleteEntry")
    public String deleteEntry(Model model, @RequestParam("entryId") long entryId,
                              @ModelAttribute("currentUser") User currentUser) {

        if (UserService.checkUserforAdmin(currentUser)) {

            entryService.deleteEntryWithComments(model, entryId);
            model.addAttribute("entries", entryRepository.findAllByOrderByCreationDateDesc());
        }

        return "redirect:/";
    }


    @PostMapping("/saveEntry")
    public String saveEntry(Model model, @ModelAttribute("entry") @Valid EntryDTO entry,
                         BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser,
                         @RequestParam("entryToEditId") long entryToEditId,
                         RedirectAttributes redirectAttributes) throws IOException {

        if (bindingResult.hasErrors()) {

            model.addAttribute("entry", new EntryDTO());
            model.addAttribute("entryToEdit", new Entry());
            model.addAttribute("categorys", entryService.categoryRepository.getAllByOrderByName());

            model.addAttribute("Error", "Gib etwas ein");

            return "entry_edit";
        } else if (!UserService.checkUserforAdmin(currentUser)) {
            return "redirect:/login";

        } else {

            Entry entryToShow;
            if (entryToEditId == 0) {

                entryToShow = entryService.createNewEntry(model, entry, currentUser);

            } else {

                entryToShow = entryService.saveEditedEntry(model, entryToEditId, entry, currentUser);
            }

            redirectAttributes.addAttribute("entryId", entryToShow.getId());
            return "redirect:/entryView";
        }

    }


}
