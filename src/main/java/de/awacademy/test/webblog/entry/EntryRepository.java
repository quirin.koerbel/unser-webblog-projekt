package de.awacademy.test.webblog.entry;

import de.awacademy.test.webblog.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface EntryRepository extends JpaRepository<Entry, Long> {

    List<Entry> findAllByOrderByCreationDateDesc();
    Entry  findFirstById(long id);
    void deleteById(long id);
    List<Entry> findAllByCategoriesContainsOrderByCreationDateDesc(Category c);


}
