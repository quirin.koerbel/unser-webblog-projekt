package de.awacademy.test.webblog.category;

import javax.validation.constraints.Size;

public class CategoryDTO {

    @Size(min = 4)
    private String nameCategory;

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
}
